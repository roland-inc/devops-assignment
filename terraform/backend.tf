terraform {
  backend "azurerm" {
    resource_group_name  = "saltoks-infra-management"
    storage_account_name = "saltoksterraformstate"
    container_name       = "terraformstate"
    key                  = "saltoksdevops.terraform.tfstate"
    use_msi              = true
  }
}
