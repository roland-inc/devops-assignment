# devops-assignment


## About this project

Roland Inc. has been tasked with creating a pipeline solution in order to deploy resources on the Azure Cloud using Terraform.

This code will use terraform to deploy a resource group and a storage account on Azure.

In order to make use of this code, you need to install the mentioned pre-requisites below.


## Pre-requisites

### GitLab Runner

The preferred choice is a GitLab managed Kubernetes cluster. This will allow the pipeline jobs to be ephemeral,
and all the sensitive info handled by them to disappear once they are done.

To create a managed Kubernetes cluster, go to the Operations section of your project.


### Azure service principal

Create an Azure service principal to allow your jobs to authenticate with Azure in a non-interactive manner.

Example:

```bash
$ az ad sp create-for-rbac -n "some_service_principal_name"
```


### Kubernetes secret to hold credentials

The credentials return by the service principal creation command above can be stored in a Kubernetes secret.

To create a secret in Kubernetes run the below command:

```bash
$ kubectl –n gitlab-managed-apps create secret generic creds --from-literal <key1>=<value1> --from-literal <key2>=<value2> ...
```

### Configure your runner to mount Kubernetes secret as a volume on pipeline job pods

Add this to your config.toml (under /etc/gitlab-runner/ if your gitlab-runner is running as root, or under ~/.gitlab-runner/ if it is not):

```bash
[[runners.kubernetes.volumes.secret]]
      name = "creds"
      mount_path = "/creds"
      read_only = true  
```

This should be under the runners.kubernetes section.


### Create Azure resource group and storage account for your Terraform state

In order to manage remote state independently from your target infrastructure, you can create a segregated resource group and storage account for this purpose. The Terraform backend will reference a storage container inside this storage account as blob storage.






